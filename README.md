<img src="assets/logo-blk.png" width="7%">
# Hello, welcome to code@mms. Take a look to get started: #
code@mms is a club focused around programming and having a community of like-minded people around you. This README file is written to explain the basic operation of the group and recommended practices. We will also list some helpful resources to help you get started. We also ask that you review our [code of conduct](CODE_OF_CONDUCT.md) carefully.  For more help, please join our Slack Workspace, but keep in mind our code of conduct and basic rules that we have outlined [here.](#slack-rules)

## Table of Contents ##

### Getting Started ###
- [Beginners](#beginners)
- [Existing Programmers](#existing-programmers)

### Other ###
- [Contributing to this repository](#contributing)
- [Recommended Software List](software.md)

## Beginners ##
 Thank you for showing interest in code@mms. We want to get you started in the best direction possible, so this guide might vary from easy to hard depending on where you are. There are a few things we would like you to know before you start.
 
 We feel like it is best to know basic computer terminology and some programming terminology. This is due to the fact that we would much rather focus on programming than tech support. Unfortunately, it isn't really our job to teach you, but we plan on adding some good resources here for learning about how computers work and some basic nerd terminology.
 
 To help make things way easier, sign up for Slack [here.](https://goo.gl/PqQz9F)
 
 To get started, we recommend jumping straight into learning a language. This might seem tedious at first, but once you get the basics down, it'll get easier. Depending on what you want to do, we have compiled a list of languages organized on their uses.
 
### Reccomended languages to learn ###
- Web Development (Good for beginners; Recommended to learn in order of list):
    - [HTML](https://www.codecademy.com/learn/learn-html)
    - [CSS](https://www.codecademy.com/learn/learn-css)
    - [JavaScript](https://www.codecademy.com/learn/introduction-to-javascript)
- Android App Development: 
    - [Java](https://www.codecademy.com/learn/learn-java)
    - Kotlin (We are scouting for some good tutorials. Bear with us.)
        - Tip: Google has a course on building Android apps [here.](https://developer.android.com/training/index.html) If you know basic Java, this is a good choice.

### Next steps ###
Obviously, there is a lot more to programming than knowing a language. Here are some other things that are good to know.
- [Command line](https://www.codecademy.com/learn/learn-the-command-line) - Using the command line is essential to being efficient with computers.
- [Git](https://www.codecademy.com/learn/learn-git) - Git is a system for managing projects and allowing for collaboration on projects.

More tutorials and different languages are available on [Codecademy.](https://codecademy.com)

## Existing Programmers ##
 Welcome to the code@mms club! Since you most likely know at least the basics of a programming language and computers in general, you won't need a lot of assistance. Here are a few things we would like you to know so you can work efficiently.
- If you need to install a program ask Ms. Forrest for the admin password to recieve it.
- Signing up for the Slack Workspace is easy! Just click [here.](https://goo.gl/PqQz6F)
- Check the recommended sotware list [here.](software.md)

From this point on, you should be able to work on any project you like. If you don't know what to work on, please check the [projects.md file.](projects.md)

## Contributing
To contribute to this document or other documents in the get-started repository, just start a pull request and we'll review and merge your changes.